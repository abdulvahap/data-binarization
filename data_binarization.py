"""
This module converts numpy data into binary format to be deployed to microcontroller

Author: Vahap
Date: 13-12-2021
"""
import os
from os import path
import numpy as np


def convert_data_into_binary(folder_name: str = None,
                             new_folder_name: str = "binary_files") -> None:
    """
    Convert numpy data into binary format

    Keyword arguments:
    :param folder_name: name of the folder that has numpy data
    :param new_folder_name: folder name where binary format data will be stored in
    """
    if not path.exists(new_folder_name):
        # Create a new folder
        os.mkdir(new_folder_name)
    if folder_name is not None:
        # Retrieve file names to loop
        file_names = os.listdir(folder_name)
        # Loop through each file
        for file_name in file_names:
            complete_path = os.path.join(folder_name, file_name)
            # Load numpy file to be saved as binary
            numpy_file = np.load(complete_path)
            new_file_name = os.path.join(new_folder_name, file_name[:-4])
            # Save new binary file
            numpy_file.tofile(f"{new_file_name}")


# Converts data into binaries
convert_data_into_binary(folder_name="test_X", new_folder_name="X")
convert_data_into_binary(folder_name="test_Y", new_folder_name="Y")
