# Data Binarization

This module converts numpy data into binary format files to be deployed in microcontrollers.

The script requires import of linux 'os' and installation of 'numpy' within the Python environment.

This file contains the following functions:

```python
import os
from os import path
import numpy as

# Converts data into binaries
convert_data_into_binary(folder_name="test_X", new_folder_name="X")
convert_data_into_binary(folder_name="test_Y", new_folder_name="Y")
```